package cn.chrilwe.pigpig.search;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import cn.chrilwe.zk.starter.annotations.EnableZkClient;

/**
 * 
 * @author chrilwe
 * 2020-7-26
 */
@SpringBootApplication
@EnableZkClient
public class Application {
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	
}
