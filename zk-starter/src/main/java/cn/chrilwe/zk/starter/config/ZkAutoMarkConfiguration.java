package cn.chrilwe.zk.starter.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
/**
 * 
 * @author chrilwe
 * 2020-7-26
 */
@Configuration
public class ZkAutoMarkConfiguration {
	
	public class Mark {
		
	}
	
	@Bean
	public Mark mark() {
		
		return new Mark();
	}

}
