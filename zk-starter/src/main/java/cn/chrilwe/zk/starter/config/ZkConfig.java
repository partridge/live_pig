package cn.chrilwe.zk.starter.config;

import lombok.Data;

/**
 * 
 * @author chrilwe
 * 2020-7-26
 */
public class ZkConfig {
	
	protected volatile ZkConfig zkConfig;
	
	protected String connectString;
	
	protected String applicationName;
	
	protected int sessionTimeout;
	
	protected String serverHostAndPort;
	
	public String getServerHostAndPort() {
		return serverHostAndPort;
	}

	public void setServerHostAndPort(String serverHostAndPort) {
		this.serverHostAndPort = serverHostAndPort;
	}

	public int getSessionTimeout() {
		return sessionTimeout;
	}

	public void setSessionTimeout(int sessionTimeout) {
		this.sessionTimeout = sessionTimeout;
	}

	public String getConnectString() {
		return connectString;
	}

	public void setConnectString(String connectString) {
		this.connectString = connectString;
	}

	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	private ZkConfig() {
		
	}
	
	public ZkConfig build() {
		if(zkConfig == null) {
			synchronized (zkConfig) {
				if(zkConfig == null) {
					return new ZkConfig();
				}
			}
		}
		return zkConfig;
	}
	
	
}
