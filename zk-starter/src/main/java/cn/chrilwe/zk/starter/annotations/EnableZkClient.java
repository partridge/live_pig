package cn.chrilwe.zk.starter.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.List;

import org.springframework.context.annotation.Import;

import cn.chrilwe.zk.starter.config.ZkAutoMarkConfiguration;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(ZkAutoMarkConfiguration.class)
public @interface EnableZkClient {
	
}
