package cn.chrilwe.zk.starter.test;

import java.util.concurrent.CountDownLatch;

import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.Watcher.Event.KeeperState;
import org.apache.zookeeper.ZooKeeper;

public class ZkClient {

	public static void main(String[] args) throws Exception {
		CountDownLatch cdl = new CountDownLatch(1); 
		
		ZooKeeper zk = new ZooKeeper("192.168.43.163:2181", 20, new Watcher() {
			
			@Override
			public void process(WatchedEvent event) {
				if(event.getState().equals(KeeperState.SyncConnected)) {
					cdl.countDown();
				}
			}
		});
		cdl.await();
		System.out.println("连接成功");
		zk.create("/hi", "".getBytes(), null, CreateMode.EPHEMERAL_SEQUENTIAL);
	}
}
