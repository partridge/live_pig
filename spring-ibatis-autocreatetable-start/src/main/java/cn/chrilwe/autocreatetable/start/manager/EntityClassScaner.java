package cn.chrilwe.autocreatetable.start.manager;
/**
 *  扫描entity注解类
 * @author chrilwe
 * 2020-8-6
 */

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.core.type.classreading.CachingMetadataReaderFactory;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.MetadataReaderFactory;

public class EntityClassScaner {

	public List<String> scan(String entityPath) throws IOException {
		
		return doScan(entityPath);
	}
	
	protected List<String> doScan(String entityPath) throws IOException {
		ResourcePatternResolver resolver =new PathMatchingResourcePatternResolver();
		MetadataReaderFactory metaReader = new CachingMetadataReaderFactory(resolver);
		Resource[] resources = resolver.getResources("classpath*:"+ entityPath +"**/*.class");
		
		List<String> classNames = new ArrayList<String>();
		for (Resource resource : resources) {
			MetadataReader metadataReader = metaReader.getMetadataReader(resource);
			String className = metadataReader.getClassMetadata().getClassName();
			classNames.add(className);
		}
		return classNames;
	}
}
