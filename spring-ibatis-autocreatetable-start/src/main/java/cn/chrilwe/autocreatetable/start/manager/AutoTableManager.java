package cn.chrilwe.autocreatetable.start.manager;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.springframework.util.StringUtils;

import cn.chrilwe.autocreatetable.start.statement.ColumnStatement;
import cn.chrilwe.autocreatetable.start.statement.TableStatement;

/**
 * 
 * @author chrilwe 2020-8-6
 */
public class AutoTableManager {

	public static void create(String entityPath, Connection connection) throws IOException, ClassNotFoundException, SQLException {
		EntityClassScaner scaner = new EntityClassScaner();
		List<String> classNames = scaner.scan(entityPath);
		if (classNames != null && classNames.size() > 0)
			for (String className : classNames) {
				StatementParser sp = new StatementParser();
				TableStatement statement = sp.getStatement(Class.forName(className));
				if (statement != null)
					try {
						doCreate(statement, connection);
					} catch (Exception e) {
						if(e.getMessage().contains("exists")) {
							continue;
						} else {
							throw e;
						}
					}
			}
	}

	protected static void doCreate(TableStatement statement, Connection connection) throws SQLException {
		String tableName = statement.getTableName();
		String tableEngine = statement.getTableEngine();
		String tableCharset = statement.getTableCharset();
		List<ColumnStatement> columnStatements = statement.getColumnStatements();

		String pre = "create table " + tableName + "(";

		String end = ")engine=" + tableEngine + " charset=" + tableCharset + ";";

		String center = "";
		String primaryKey = null;
		String autoIncrementKey = null;
		if (columnStatements != null && columnStatements.size() > 0) {
			for (int i = 0; i < columnStatements.size(); i++) {
				if (!StringUtils.isEmpty(primaryKey)) {
					throw new RuntimeException("too many primary key");
				}
				ColumnStatement cs = columnStatements.get(i);
				String columnName = cs.getColumnName();
				int columnLen = cs.getColumnLen();
				String columnType = cs.getColumnType();
				boolean primary = cs.isPrimary();
				if (primary) {
					boolean autoIncrement = cs.isAutoIncrement();
					if (autoIncrement) {
						autoIncrementKey = columnName;
					}
					primaryKey = columnName;
				}
				boolean isNull = cs.isNull();
				if (i == columnStatements.size() - 1) {
					center += columnName + " " + columnType + "(" + columnLen + ")" + (isNull ? "" : "not null")
							+ (((autoIncrementKey == null) && cs.isAutoIncrement() && (primaryKey == null) && primary)
									? " auto_increment"
									: "") + (primaryKey==null?"":",primary key("+primaryKey+")");
				} else {
					center += columnName + " " + columnType + "(" + columnLen + ")" + (isNull ? "" : "not null")
							+ (((autoIncrementKey == null) && cs.isAutoIncrement() && (primaryKey == null) && primary)
									? " auto_increment"
									: "") + ",";
				}
			}
		}
		String sql = pre + center + end;
		Statement s = connection.createStatement();
		System.out.println(sql);
		boolean execute = s.execute(sql);
	}
}
