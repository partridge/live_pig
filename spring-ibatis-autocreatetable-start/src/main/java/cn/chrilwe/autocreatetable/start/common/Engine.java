package cn.chrilwe.autocreatetable.start.common;
/**
 * 
 * @author chrilwe
 * 2020-8-6
 */
public enum Engine {
	INNODB("innodb", 1), MYSAM("myisam", 2);

	private String name;
	
	private int index;
	
	private Engine(String name, int index) {
		this.name = name;
		this.index = index;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}
}
