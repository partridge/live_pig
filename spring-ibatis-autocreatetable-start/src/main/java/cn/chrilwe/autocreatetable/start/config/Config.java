package cn.chrilwe.autocreatetable.start.config;
/**
 * 
 * @author chrilwe
 * 2020-8-6
 */

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:ibatis.properties")
public class Config {

	@Bean
	public Mark mark() {
		return new Mark();
	}
	
	public class Mark {
		
	}
}
