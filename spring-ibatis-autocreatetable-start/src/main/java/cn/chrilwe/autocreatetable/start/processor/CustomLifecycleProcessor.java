package cn.chrilwe.autocreatetable.start.processor;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.support.DefaultLifecycleProcessor;
import org.springframework.stereotype.Component;

import cn.chrilwe.autocreatetable.start.config.Config.Mark;
import cn.chrilwe.autocreatetable.start.manager.AutoTableManager;

/**
 * spring容器完成初始化，执行建表逻辑后置处理器
 * @author chrilwe
 * 2020-8-6
 */
@Component("lifecycleProcessor")
@ConditionalOnBean(Mark.class)
public class CustomLifecycleProcessor extends DefaultLifecycleProcessor {

	@Autowired
	private DataSource dataSource;
	
	@Value("${ibatis.entity.path}")
	private String entityPath; 
	
	@Override
	public void onRefresh() {
		try {
			Connection connection = dataSource.getConnection();
			AutoTableManager.create(entityPath, connection);
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


}
