package cn.chrilwe.autocreatetable.start.statement;
/**
 * 
 * @author chrilwe
 * 2020-8-6
 */

import java.util.List;

public class TableStatement {

	private String tableName;
	
	private String tableEngine;
	
	private String tableCharset;
	
	private List<ColumnStatement> columnStatements;

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getTableEngine() {
		return tableEngine;
	}

	public void setTableEngine(String tableEngine) {
		this.tableEngine = tableEngine;
	}

	public String getTableCharset() {
		return tableCharset;
	}

	public void setTableCharset(String tableCharset) {
		this.tableCharset = tableCharset;
	}

	public List<ColumnStatement> getColumnStatements() {
		return columnStatements;
	}

	public void setColumnStatements(List<ColumnStatement> columnStatements) {
		this.columnStatements = columnStatements;
	}

}
