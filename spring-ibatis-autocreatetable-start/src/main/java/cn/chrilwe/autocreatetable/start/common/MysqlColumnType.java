package cn.chrilwe.autocreatetable.start.common;

public enum MysqlColumnType {
	
	INT("int", 1), VARCHAR("varchar", 2), DATETIME("datetime", 3);
	
	private String name;
	
	private int index;
	
	private MysqlColumnType(String name, int index) {
		this.name = name;
		this.index = index;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

}
