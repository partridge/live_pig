package cn.chrilwe.pigpig.im.ex;

import cn.chrilwe.pigpig.core.ex.BaseEx;
/**
 * 
 * @author chrilwe
 * 2020-7-25
 */
public class WsUrlNotSetEx extends BaseEx {

	private int code;

	public WsUrlNotSetEx() {
		super();
	}

	public WsUrlNotSetEx(String msg, int code) {
		super(msg, code);
	}

}
