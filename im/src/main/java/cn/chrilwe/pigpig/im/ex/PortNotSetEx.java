package cn.chrilwe.pigpig.im.ex;

import cn.chrilwe.pigpig.core.ex.BaseEx;

public class PortNotSetEx extends BaseEx {
	
	private int code;

	public PortNotSetEx() {
		super();
	}

	public PortNotSetEx(String msg, int code) {
		super(msg, code);
	}
}
