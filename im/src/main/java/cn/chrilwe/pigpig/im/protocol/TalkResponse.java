package cn.chrilwe.pigpig.im.protocol;

import lombok.Data;

/**
 * 
 * @author chrilwe 2020-7-27
 */
@Data
public class TalkResponse {

	// 响应状态
	private int status;

	// 响应消息
	private String resMsg;
}
