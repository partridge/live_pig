package cn.chrilwe.pigpig.im.server;

import cn.chrilwe.pigpig.im.config.ServerConfig;
import cn.chrilwe.pigpig.im.ex.ServerConfigNullPointerEx;

/**
 * 
 * @author chrilwe
 * 2020-7-25
 */
public interface Server {
	
	public void start();
	
}
