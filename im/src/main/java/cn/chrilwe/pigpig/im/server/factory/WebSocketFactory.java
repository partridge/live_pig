package cn.chrilwe.pigpig.im.server.factory;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.ApplicationContext;

import cn.chrilwe.pigpig.im.config.ServerConfig;
import cn.chrilwe.pigpig.im.ex.PortNotSetEx;
import cn.chrilwe.pigpig.im.ex.ServerConfigNullPointerEx;
import cn.chrilwe.pigpig.im.ex.WsUrlNotSetEx;
import cn.chrilwe.pigpig.im.filters.Filter;
import cn.chrilwe.pigpig.im.server.Server;
import cn.chrilwe.pigpig.im.server.WebSocketServer;

/**
 * 
 * @author chrilwe
 * 2020-7-25
 * 
 */
public class WebSocketFactory implements AbstractFactory {

	@Override
	public Server getServer(ServerConfig serverConfig, ApplicationContext ac, List<Filter> filters) {
		if(serverConfig == null) {
			throw new ServerConfigNullPointerEx("serverconfig can not be null", 200);
		}
		int port = serverConfig.getPort();
		if(port <= 0) {
			throw new PortNotSetEx("port is not set",201);
		}
		String wsUrl = serverConfig.getWsUrl();
		if(StringUtils.isEmpty(wsUrl)) {
			throw new WsUrlNotSetEx("ws url not set", 202);
		}
		return new WebSocketServer(serverConfig, ac, filters);
	}

}
