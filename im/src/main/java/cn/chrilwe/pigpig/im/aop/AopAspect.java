package cn.chrilwe.pigpig.im.aop;

import java.lang.reflect.Parameter;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import cn.chrilwe.pigpig.im.protocol.TalkRequest;
import cn.chrilwe.pigpig.im.protocol.TalkResponse;

/**
 *  aop切面
 * @author chrilwe
 * 2020-7-31
 */
@Aspect
@Component
public class AopAspect {
	
	@Pointcut(value="@annotation(cn.chrilwe.pigpig.im.annotations.CheckLogin)")
	public void pointCut() {}
	
	@Around(value = "pointCut()")
	public Object around(ProceedingJoinPoint joinPoint) {
		try {
			Object[] args = joinPoint.getArgs();
			TalkRequest talkRequest = (TalkRequest)args[0];
			boolean checkLogin = checkLogin(talkRequest.getToken());
			if(!checkLogin) {
				TalkResponse talkResponse = new TalkResponse();
				talkResponse.setStatus(401);
				talkResponse.setResMsg("unauthorizate");
				return talkResponse;
			}
			Object proceed = joinPoint.proceed();
			return proceed;
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	protected boolean checkLogin(String token) {
		System.out.println("检查用户是否登录, token="+token);
		return true;
	}
}
