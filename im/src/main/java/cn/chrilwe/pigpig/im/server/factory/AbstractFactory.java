package cn.chrilwe.pigpig.im.server.factory;

import java.util.List;

import org.springframework.context.ApplicationContext;

import cn.chrilwe.pigpig.im.config.ServerConfig;
import cn.chrilwe.pigpig.im.filters.Filter;
import cn.chrilwe.pigpig.im.server.Server;

/**
 * 
 * @author chrilwe
 * 2020-7-25
 */
public interface AbstractFactory {
	public Server getServer(ServerConfig serverConfig, ApplicationContext ac, List<Filter> filters);
}
