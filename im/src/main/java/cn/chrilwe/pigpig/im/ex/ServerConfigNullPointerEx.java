package cn.chrilwe.pigpig.im.ex;

import cn.chrilwe.pigpig.core.ex.BaseEx;

/**
 * 
 * @author chrilwe
 * 2020-7-25
 */
public class ServerConfigNullPointerEx extends BaseEx {
	
	private int code;

	public ServerConfigNullPointerEx() {
		super();
	}

	public ServerConfigNullPointerEx(String msg, int code) {
		super(msg, code);
	}

}
