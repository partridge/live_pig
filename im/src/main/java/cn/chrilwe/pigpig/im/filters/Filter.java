package cn.chrilwe.pigpig.im.filters;
/**
  *  过滤器
 * @author chrilwe
 * 2020-7-28
 */

import cn.chrilwe.pigpig.im.protocol.TalkRequest;
import cn.chrilwe.pigpig.im.protocol.TalkResponse;

public interface Filter {
	public TalkResponse doChain(TalkRequest talkRequest);
}
