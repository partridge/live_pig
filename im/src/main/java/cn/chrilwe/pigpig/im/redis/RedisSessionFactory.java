package cn.chrilwe.pigpig.im.redis;

import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.JedisPool;

public class RedisSessionFactory implements RedisSession {
	
	private RedisConfig redisConfig;
	
	private ThreadLocal<JedisCluster> threadLocal = new ThreadLocal<JedisCluster>();
	
	public RedisSessionFactory(RedisConfig redisConfig) {
		this.redisConfig = redisConfig;
	}

	@Override
	public JedisPool getJedisPool() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public JedisCluster getJedisCluster() {
		if(redisConfig == null) {
			throw new RuntimeException("RedisConfig can not be null");
		}
		
		return null;
	}

	@Override
	public String get(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void set(String key, String value) {
		// TODO Auto-generated method stub
		
	}


}
