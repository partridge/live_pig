package cn.chrilwe.pigpig.im.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * 
 * @author chrilwe
 * 2020-7-28
 */
@Configuration
@EnableAspectJAutoProxy
public class SpringConfig {


}
