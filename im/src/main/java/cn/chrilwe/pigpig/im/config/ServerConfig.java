package cn.chrilwe.pigpig.im.config;

import lombok.Data;

/**
 * 
 * @author chrilwe
 * 2020-7-25
 * server配置参数
 */
@Data
public class ServerConfig {
	
	private static volatile ServerConfig serverConfig;
	
	private int port;
	
	private String wsUrl;
	
	
	private ServerConfig() {
		
	}
	
	public static ServerConfig build() {
		if(serverConfig == null) {
			synchronized (ServerConfig.class.getClassLoader()) {
				if(serverConfig == null) {
					return new ServerConfig();
				}
			}
		}
		return serverConfig;
	}
	
}
