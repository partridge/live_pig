package cn.chrilwe.pigpig.im.server.handler;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;
import io.netty.handler.stream.ChunkedWriteHandler;

/**
 * 
 * @author chrilwe
 * 2020-7-25
 */
@SuppressWarnings("rawtypes")
public class ServerChannelInilizer extends ChannelInitializer {
	
	String url;
	
	public ServerChannelInilizer(String url) {
		this.url = url;
	}

	@Override
	protected void initChannel(Channel ch) throws Exception {
		ChannelPipeline pipeline = ch.pipeline();
		pipeline.addLast("httpServerCodec", new HttpServerCodec());
		pipeline.addLast("chunkedWriteHandler", new ChunkedWriteHandler());
		pipeline.addLast("httpObjectAggregator", new HttpObjectAggregator(536));
		// WebSocketServerProtocolHandler中实现了对websocket的封装，不用我们去考虑握手的问题
		pipeline.addLast("webSocketServerProtocolHandler", (ChannelHandler) new WebSocketServerProtocolHandler(url));
		pipeline.addLast("textWebSocketFrameToTalkProtoDecoder", new TextWebSocketFrameToTalkProtoDecoder());
		pipeline.addLast("messageHandler", new MessageHandler());
	}


}
