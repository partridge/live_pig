package cn.chrilwe.pigpig.im.service;
/**
 * 
 * @author chrilwe
 * 2020-7-26
 */

import java.util.Map;

import org.springframework.stereotype.Service;

import cn.chrilwe.pigpig.core.base.Code;
import cn.chrilwe.pigpig.im.annotations.CheckLogin;
import cn.chrilwe.pigpig.im.protocol.TalkRequest;
import cn.chrilwe.pigpig.im.protocol.TalkResponse;

@Service
public class TopicMessageService {
	
	//发送消息
	@CheckLogin
	public TalkResponse topicMsg(TalkRequest talkRequest) {
		String msg = talkRequest.getMsg();
		String msg1 = parseAndCheckMsg(msg);
		
		TalkResponse res = new TalkResponse();
		res.setStatus(Code.SUCCESS);
		String resMsg = "<div style=\"width:28px;height:20px;font-size:12px;background-color:#FF9933;text-align:center;display:inline;\">房管</div>&#32<img src=\"../../static/gongjue.png\" style=\"width:28px;height:25px;\"></img>&#32<div class=\"UserLevel\">50</div>&#32<div style=\"height:20px;color:#CC33FF;display:inline;\">xiaobai:</div>&#32<div style=\"height:20px;color:black;word-wrap:break-word;word-break:break-all;white-space:normal;display:inline;font-size:12px;\">"+msg1+"</div>";
		res.setResMsg(resMsg);
		return res;
	}
	
	//对消息进行解析，防止一些crsf攻击
	protected String parseAndCheckMsg(String msg) {
		String msg1 = msg.replaceAll("<", "&#60");
		String msg2 = msg1.replaceAll(">", "&#62");
		
		return msg2;
	}
	
}
