package cn.chrilwe.pigpig.im.server.handler;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.alibaba.fastjson.JSON;

import cn.chrilwe.pigpig.im.protocol.TalkRequest;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageDecoder;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author chrilwe
 * 2020-7-25
 */
public class TextWebSocketFrameToTalkProtoDecoder extends MessageToMessageDecoder<TextWebSocketFrame> {

	@Override
	protected void decode(ChannelHandlerContext ctx, TextWebSocketFrame msg, List<Object> out) throws Exception {
		String text = msg.text();
		if(StringUtils.isNotEmpty(text)) {
			try {
				TalkRequest proto = JSON.parseObject(text, TalkRequest.class);
				out.add(proto);
			} catch (Exception e) {
				System.err.print("错误的消息格式");
				out.add(null);
			}
		}
	}
	
}
