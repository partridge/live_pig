package cn.chrilwe.pigpig.im.redis;

import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.JedisPool;

public interface RedisSession {

	JedisPool getJedisPool();
	
	JedisCluster getJedisCluster();
	
	public String get(String key);
	
	public void set(String key, String value);
}
