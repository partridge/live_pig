package cn.chrilwe.pigpig.im.redis;
/**
 * 
 * @author chrilwe
 * 2020-8-2
 */
public class RedisConfig {
	
	private RedisConfig redisConfig;
	
	private String serverAddress;
	
	private String username;
	
	private String password;

	private RedisConfig() {
		
	}
	
}
