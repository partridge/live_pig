package cn.chrilwe.pigpig.im.words;

import cn.chrilwe.pigpig.im.annotations.Ordered;
import cn.chrilwe.pigpig.im.filters.Filter;
import cn.chrilwe.pigpig.im.protocol.TalkRequest;
import cn.chrilwe.pigpig.im.protocol.TalkResponse;

/**
  * 不雅词过滤
 * @author chrilwe
 * 2020-7-28
 */
@cn.chrilwe.pigpig.im.annotations.Filter
@Ordered(1)
public class IndecentWordsFilter implements Filter {

	@Override
	public TalkResponse doChain(TalkRequest talkRequest) {
		
		return null;
	}

}
