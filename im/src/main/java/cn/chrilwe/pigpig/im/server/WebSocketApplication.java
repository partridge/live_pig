package cn.chrilwe.pigpig.im.server;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.alibaba.fastjson.JSON;

import cn.chrilwe.pigpig.im.Main;
import cn.chrilwe.pigpig.im.config.ServerConfig;
import cn.chrilwe.pigpig.im.config.SpringConfig;
import cn.chrilwe.pigpig.im.filters.Filter;
import cn.chrilwe.pigpig.im.scaner.ClassScanner;
import cn.chrilwe.pigpig.im.server.factory.AbstractFactory;
import cn.chrilwe.pigpig.im.server.factory.WebSocketFactory;
import cn.chrilwe.pigpig.im.words.IndecentWordsFilter;
/**
 * 
 * @author chrilwe
 * 2020-7-28
 */
public class WebSocketApplication {

	public static void run(Class<?> clazz) {
		String name = clazz.getPackage().getName();
		ApplicationContext ac = springApplicationInit(name);
		
		List<Filter> filters = null;
		try {
			filters = scanAndSortFilter(name);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		AbstractFactory serverfactory = new WebSocketFactory();
		ServerConfig serverConfig = ServerConfig.build();
		serverConfig.setPort(8888);
		serverConfig.setWsUrl("/ws");
		Server server = serverfactory.getServer(serverConfig,ac,filters);
		server.start();
	}
	
	protected static ApplicationContext springApplicationInit(String basePackage) {
		AnnotationConfigApplicationContext c = new AnnotationConfigApplicationContext();
		c.register(SpringConfig.class);
		c.scan(basePackage);
		c.refresh();
		return c;
	}
	
	protected static List<Filter> scanAndSortFilter(String basePackage) throws IOException, InstantiationException, IllegalAccessException {
		List<String> scan = ClassScanner.scan(basePackage);
		
		
		
		List<Filter> l = new ArrayList<Filter>();
		for (String className : scan) {
			try {
				Class<?> clazz = Class.forName(className);
				
				boolean flag = checkClazzSuperclassIsFilter(clazz);
				if(flag) {
					l.add((Filter)clazz.newInstance());
				}
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return l;
	}
	
	protected static void quickSort(Integer[] l,int left, int right) {
		if(left < right) {
			Integer privot = l[left];
			int begin = left;
			int end = right;
			while(begin < end) {
				while(begin < end && l[end]>privot) {
					end--;
				}
				l[begin] = l[end];
				
				while(begin < end && l[begin]<privot) {
					begin++;
				}
				l[end] = l[begin];
			}
			l[begin] = privot;
			quickSort(l, left, begin-1);
			quickSort(l, begin+1, right);
		} else {
			return;
		}
	}
	
	protected static boolean checkClazzSuperclassIsFilter(Class<?> clazz) {
		cn.chrilwe.pigpig.im.annotations.Filter filter = clazz.getAnnotation(cn.chrilwe.pigpig.im.annotations.Filter.class);
		if(filter != null) {
			Type[] t = clazz.getGenericInterfaces();
			for (int i = 0; i < t.length; i++) {
				if(t[i].getTypeName().equals("cn.chrilwe.pigpig.im.filters.Filter")) {
					return true;
				}
			}
		}
		return false;
	}
	
	public static void main(String[] args) {
		Integer[] i = {17,10,3,19,8,4,22};
		quickSort(i,0, i.length-1);
		System.out.println(JSON.toJSONString(i));
	}
}
