package cn.chrilwe.pigpig.im;

import cn.chrilwe.pigpig.im.config.ServerConfig;
import cn.chrilwe.pigpig.im.server.Server;
import cn.chrilwe.pigpig.im.server.WebSocketApplication;
import cn.chrilwe.pigpig.im.server.factory.AbstractFactory;
import cn.chrilwe.pigpig.im.server.factory.WebSocketFactory;

/**
 * 
 * @author chrilwe
 * 2020-7-25
 */
public class Main {
	
	public static void main(String[] args) {
		WebSocketApplication.run(Main.class);
	}
	
}
