package cn.chrilwe.pigpig.im.protocol;

import lombok.Data;

/**
  * 自定义弹幕消息格式协议
 * @author chrilwe
 * 2020-7-26
 */
@Data
public class TalkRequest {
	//身份令牌
	private String token;
	
	//消息文本
	private String msg;
	
	//房间号
	private int roomId;
	
}
