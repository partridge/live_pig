package cn.chrilwe.pigpig.im.test;

import sun.misc.Unsafe;

public class MyUnsafe {

	private static final Unsafe unsafe = Unsafe.getUnsafe();

	private static int num = 0;

	public static void main(String[] args) throws NoSuchFieldException, SecurityException {
		MyUnsafe mu = new MyUnsafe();
		long offset = unsafe.objectFieldOffset(MyUnsafe.class.getDeclaredField("num"));
		Thread thread1 = new Thread(new Runnable() {

			@SuppressWarnings("restriction")
			@Override
			public void run() {
				boolean res = unsafe.compareAndSwapInt(mu, offset, 0, 1);
			}
		});
		Thread thread2 = new Thread(new Runnable() {

			@SuppressWarnings("restriction")
			@Override
			public void run() {
				boolean res = unsafe.compareAndSwapInt(mu, offset, 0, 1);
			}
		});
		thread1.start();
		thread2.start();
	}
}
