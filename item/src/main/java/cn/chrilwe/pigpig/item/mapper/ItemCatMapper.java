package cn.chrilwe.pigpig.item.mapper;

import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;

/**
 * 
 * @author chrilwe
 * 2020-7-22
 */
public interface ItemCatMapper {
	
	public void selectAll();
	
}
