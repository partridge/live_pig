package cn.chrilwe.pigpig.core.ex;
/**
 * 
 * @author chrilwe
 * 2020-7-25
 */
public class BaseEx extends RuntimeException {
	
	private int code;

	public BaseEx() {
		super();
	}

	public BaseEx(String msg, int code) {
		super(msg);
		this.code = code;
	}

}
