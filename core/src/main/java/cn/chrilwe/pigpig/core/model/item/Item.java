package cn.chrilwe.pigpig.core.model.item;
/**
 * 
 * @author chrilwe
 * 2020-7-21
  * 商品信息
 */

import java.util.Date;
import java.util.List;

public class Item {
	private int id;
	
	private String title;
	
	private long price;//精度为：分
	
	private long oldPrice;
	
	private int status;
	
	private Date createTime;
	
	private int storage;
	
	private List<ItemAttribute> itemAttributes;
	
	private List<ItemDesc> itemDescs;
}
