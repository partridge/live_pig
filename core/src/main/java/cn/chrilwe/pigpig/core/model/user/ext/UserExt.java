package cn.chrilwe.pigpig.core.model.user.ext;

import java.util.List;

import cn.chrilwe.pigpig.core.model.user.Authority;
import cn.chrilwe.pigpig.core.model.user.Role;
import cn.chrilwe.pigpig.core.model.user.User;
import lombok.Data;

/**
 * 
 * @author chrilwe
 * 2020-7-30
 */
@Data
public class UserExt extends User {
	private List<Role> roles;
	private List<Authority> authorities;
}
