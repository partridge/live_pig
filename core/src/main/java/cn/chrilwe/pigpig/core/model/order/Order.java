package cn.chrilwe.pigpig.core.model.order;
/**
 * 
 * @author chrilwe
 * 2020-7-21
  *   订单表
 */

import java.util.Date;
import java.util.List;

public class Order {
	private int id;
	
	private String orderId;
	
	private int status;
	
	private Date createTime;
	
	private Date updateTime;
	
	private String orderDesc;
	
	private int userId;
	
	private List<OrderItem> orderItems;
}
