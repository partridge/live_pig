package cn.chrilwe.pigpig.core.model.item.room;

import lombok.Data;

/**
 *  房间详情
 * @author chrilwe
 * 2020-8-2
 */
@Data
public class VideoRoom {
	private int roomId;
	private int anthorId;
	private int videoCatId;
	private int onlinePerson;
	private String videoUrl;
	private int status; //0：下线    1：在线
}
