package cn.chrilwe.pigpig.core.model.user;

import java.util.Date;

import lombok.Data;

/**
  * 主播
 * @author chrilwe
 * 2020-7-30
 */
@Data
public class Anthor {
	private int id;
	private int userId;
	private long roomId;
	private String roomAddress;
	private Date createTime;
	private Date lastOnlineTime;
	private int status;
}
