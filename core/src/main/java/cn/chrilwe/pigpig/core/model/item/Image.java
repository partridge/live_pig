package cn.chrilwe.pigpig.core.model.item;
/**
 * 
 * @author chrilwe
 * 2020-7-22
  * 图片
 */
public class Image {
	private int id;

	private String originName;

	private String name;

	private long size;

	private String type;

	private String imageUrl;
}
