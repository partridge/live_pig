package cn.chrilwe.pigpig.core.base;
/**
 * 
 * @author chrilwe
 * 2020-7-26
 */
public class Code {
	
	public static int SUCCESS = 200;
	
	public static int PROTOCOL_ERR = 500;
}
