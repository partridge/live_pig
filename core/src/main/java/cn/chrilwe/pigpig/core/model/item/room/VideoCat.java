package cn.chrilwe.pigpig.core.model.item.room;

import lombok.Data;

/**
 * 视频分类
 * @author chrilwe
 * 2020-8-2
 */
@Data
public class VideoCat {
	private int catId;
	private String name;
	private int level;//当前级别
	private int parentId;
	private int orderBy;//排序序号
}
