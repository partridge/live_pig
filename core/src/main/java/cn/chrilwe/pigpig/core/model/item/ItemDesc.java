package cn.chrilwe.pigpig.core.model.item;
/**
 * 
 * @author chrilwe
 * 2020-7-21
  * 商品详情描述
 */

import java.util.List;

public class ItemDesc {
	private int id;
	
	private int itemId;
	
	private String text;//文字描述
	
	private List<ItemDescImage> itemDescImages;
}
