package cn.chrilwe.pigpig.core.model.user.ext;

import java.util.List;

import cn.chrilwe.pigpig.core.model.user.AnthorGift;
import cn.chrilwe.pigpig.core.model.user.User;
import lombok.Data;

@Data
public class AnthorExt extends User {
	private List<AnthorGift> gifts;
}
