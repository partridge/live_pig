package cn.chrilwe.pigpig.core.model.user;

import java.util.Date;

import lombok.Data;
import lombok.ToString;

/**
 * 
 * @author chrilwe
 * 2020-7-30
 */
@Data
@ToString
public class User {
	private int id;
	private String username;
	private String password;
	private String nickName;
	private int gender;// 0:男 1:女
	private int level;
	private int status;// 0:异常 1:正常
	private Date createTime;
	private String phone;
	private String email;
	private Date birthday;
	private String address;
}
