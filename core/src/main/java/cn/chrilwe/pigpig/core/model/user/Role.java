package cn.chrilwe.pigpig.core.model.user;

import lombok.Data;

/**
 * 
 * @author chrilwe
 * 2020-7-30
 */
@Data
public class Role {
	private int id;
	private String name;
}
