package cn.chrilwe.pigpig.core.model.user;

import lombok.Data;

/**
  * 主播礼物
 * @author chrilwe
 * 2020-7-30
 */
@Data
public class AnthorGift {
	private int id; //itemId+userId
	private int itemId;
	private long number;
	private int userId;
}
