package cn.chrilwe.pigpig.core.model.search;

import lombok.Data;

/**
 * 搜索房间信息
 * @author chrilwe
 * 2020-8-2
 */
@Data
public class RoomSearchDetail {
	private int roomId;
	private int anthorId;
	private String anthorName;
	private String anthorPic;//主播头像
	private String anthorRoomPic;//主播房间封面
	private String roomTitle;
	private int onlinePerson;//在线观看人数
	private int videoCatId;//视频分类
}
